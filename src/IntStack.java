/**
 * int用スタック
 */
public class IntStack{
	int data[]; // データ保存用の配列
	int top; 	// データの個数、次にPUSHする場所を示す
	private int max;	// データの最大個数の制限
	private static int LimitSize = 100;
	// 例外
	public class EmptyStackException extends RuntimeException{
		public EmptyStackException(){}
	}
	// 例外
	public class OverflowIntStackException extends RuntimeException{
		public OverflowIntStackException(){}
	}
	
	/**
	 * コンストラクタ
	 * @param size
	 */
	public IntStack(){
		this(LimitSize);
	}
	public IntStack(int size){
		top = 0;
		max = size;
		try {
			data = new int[max];		// データ領域の確保
		}catch (OutOfMemoryError e){	// 確保できない場合の例外処理
			max = 0;
		}
	} 
	
	/**
	 * スタックが空かどうかを判定します。
	 * @return
	 */
	public boolean empty(){
		// TODO ここに作成
			return false;
	}
	/**
	 * スタックの先頭にオブジェクトを入れます。
	 * @param x
	 */
	public int push(int x) throws RuntimeException{
		if (top >= max)
			throw new RuntimeException();
		// ここに作成
		return 0; 
	}
	
	/**
	 * スタックの先頭のオブジェクトを削除し、そのオブジェクトを関数の値として返します
	 * @return
	 */
	public int pop() throws RuntimeException{
		if (top <=0)
			throw new RuntimeException();
		return 0; // ここに作成（操作をよく考える）
	}
	
	/**
	 * スタックの先頭にあるオブジェクトを取り出します。
	 * @return
	 */
	public int peek(){
		if (top <=0)
			throw new RuntimeException();
		return data[top-1];
	}
	
	/**
	 * このスタックにあるオブジェクトの位置を 1 から始まるインデックスで返します。
	 * @param x
	 * @return
	 */
	public int search(int x){
		for (int i=0 ; i<top;i++)
			if(data[i]==x)return i+1;
		return -1;	// 見つからない
	}
	// スタックの内容を文字列として出力する
	public String toString(){
		String buf = "";
		for (int i=0 ; i<top;i++)
			buf += data[i]+" ";
		return buf;
	}
	
}
