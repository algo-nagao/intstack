public class StackSampleMain {

	public static void main(String[] args) {
		IntStack stack = new IntStack(10);

		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.pop();
		stack.push(4);
		stack.pop();
		stack.push(5);
		
		System.out.println("Stack is empty:"+stack.toString()+stack.empty());
		
		System.out.println("Top of Stack:"+stack.peek());
		System.out.println("POP:"+stack.pop());
		System.out.println("POP:"+stack.pop());
		System.out.println("POP:"+stack.pop());
	}
}
