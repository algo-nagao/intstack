import static org.junit.Assert.*;
import org.junit.Test;

public class IntStackTest  {
	/*
	 * 'IntStack.empty()' のためのテスト・メソッド
	 */
	@Test
	public void Empty() {
		IntStack aStack = new IntStack();
		
		boolean flag = aStack.empty();
		assertEquals(true,flag);
		aStack.push(10);
		flag = aStack.empty();
		assertEquals(false,flag);
	}
	
	/*
	 * 'IntStack.push(int)' のためのテスト・メソッド
	 */
	@Test
	public void Push() {
		IntStack aStack = new IntStack();
		
		aStack.push(10);
		assertEquals(10,aStack.data[0]);
		assertEquals(1,aStack.top);
		aStack.push(20);
		assertEquals(10,aStack.data[0]);
		assertEquals(20,aStack.data[1]);
		assertEquals(2,aStack.top);
	}
	
	/*
	 * 'IntStack.pop()' のためのテスト・メソッド
	 */
	@Test
	public void Pop() {
		IntStack aStack= new IntStack();
		
		aStack.push(10);
		aStack.push(20);
		aStack.push(30);
		
		assertEquals(30,aStack.pop());
		assertEquals(2,aStack.top);
		assertEquals(20,(int)aStack.pop());
		assertEquals(1,aStack.top);
	}
	
	/*
	 * 'IntStack.peek()' のためのテスト・メソッド
	 */
	@Test
	public void Peek() {
		IntStack data = new IntStack();
		
		data.push(10);
		data.push(20);
		data.push(30);
		
		assertEquals(30,data.peek());
		
		data.push(40);
		assertEquals(40,data.peek());
	}
	
	/*
	 * 'IntStack.search(int)' のためのテスト・メソッド
	 */
	@Test
	public void Search() {
		IntStack data = new IntStack();
		data.push(10);
		data.push(20);
		data.push(30);
		data.push(40);
		data.push(50);
		data.push(60);
		
		assertEquals(3,data.search(30));
		assertEquals(6,data.search(60));
		assertEquals(-1,data.search(100));
	}
	
}
